<?php
#================================= setting ===============================#
define ('HOST', '');      #
#=========================================================================#
### получаем реквест от робота ###
if ($_REQUEST) {
	writeToLog($_REQUEST, 'получен реквест от робота');
	### проверка статуса звонка ###
	if ($_REQUEST['ID']) {
		$lead = call('crm.lead.get', array('id' => $_REQUEST['ID']));
		writeToLog ($lead, 'лид');

		$result = call('voximplant.callback.start', array(
			'FROM_LINE'         => 'reg61550',
			'TO_NUMBER'         => $lead['result']['PHONE'][0]['VALUE'],
			'TEXT_TO_PRONOUNCE' => 'Обратный звонок работает'
		));
		writeToLog($result, 'обратный звонок');
	}
}

$result = call('voximplant.statistic.get', array('filter' => array('CALL_ID' => 'callback.c1e3413d0c8d4f06dc4c9cc042256e7a.1573478434')));
echo '<pre>';
print_r ($result);
echo'</pre>';

############################## functions #################################
function writeToLog ($data, $title = 'DEBUG') {
	$log = "\n--------------------\n";
	$log .= date('d.m.Y H:i:s')."\n";
	$log .= $title."\n";
	$log .= print_r($data, 1);
	$log .= "\n--------------------\n";

	file_put_contents('debug.txt', $log, FILE_APPEND);
	return true;
}
function call ($method, $data) {
	$result = file_get_contents(HOST.$method.'?'.http_build_query($data));
	return json_decode($result, 1);
}